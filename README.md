# Smirnoff Landing Page

Built using webpack:
SASS
JS
JQUERY
BOOTSTRAP

## Development

- Install the site locally under http://smirnoff-ice.local.
- Clone repo.
- Install dependencies. `npm install`.
- Start watch mode using `npm start`.
- Get Deploy code (minify etc...) using `npm run build`

Styles are under `/assets/styles`.
Scripts are under `/assets/scripts`.