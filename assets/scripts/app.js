'use strict';

const css = require('../styles/app.scss');

window.onload = function() {
	
  $('.js-scrollto').on('click', function(e){
    var hash = $(this).attr('href')
    var offset = ($(window).width() > 768) ? 80 : 40;
    $('html,body').animate({
      scrollTop: $(hash).offset().top - offset
    }, 800);
    return false;
  })

  $('.toggle-menu').click(function(event) {
		$(this).toggleClass('active');
		$('.main-nav').toggleClass('active');
  		return false;
  });

  $('.main-nav a').on('click', function(event) {
		$('.toggle-menu').removeClass('active');
		$('.main-nav').removeClass('active');
  });

} //window on load



